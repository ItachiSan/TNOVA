## Dependencies
- time
- glpsol (glpk)
- bzip2
- bash
- sed

## Fetch sources
- Clone this repo
- `git submodule init`
- `git submodule update`

You also need to fix the *thesis/Makefile.am*; you just need to enter
the *thesis* folder and copy the *Makefile.txt* to *Makefile.am*.

## How to build
- Copy *thesis-mk/Makefile.am* in the *thesis* folder
- `autoreconf -i`
- `./configure`
- `make`

I do suggest to use only one job to be sure to generate properly all tests.
Just use a plain `make` command or `make -j1`.

## How to start automatic tests
- Follow the "How to build"
- `make check`

Also, `make check` will work also after normal build.
Additionally, you can run tests in parallel with `make check -jNUMBER_OF_JOBS_FOR_TESTS`

## How to edit tests data
Edit (tests/Makefile.am); the top part is tests-related.
You can tweak the variables to have more or less tests.

## Tested on...
Antergos (ArchLinux)
Should theorically work on any Linux OS.

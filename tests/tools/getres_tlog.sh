#!/bin/bash
for s in 20; do
	for alpha in 1 0; do
		for beta in 1 0; do
			for gamma in 1 0; do
				for cplex in 1; do
					for ((i = 0; i<=9; i++)); do
						zcat ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | awk -f profile.awk > tlog;
						for j in `cat tlog`; do 
							echo $alpha " " $beta " " $gamma " " $i " " $j | sed 's/_/ /g'
						done;
					done;
				done;
			done;
		done;
	done;
done

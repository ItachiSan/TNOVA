#!/bin/bash
for s in 20; do
	for alpha in 1 0; do
		for beta in 1 0; do
			for gamma in 1 0; do
				for cplex in 1; do
					for ((i = 0; i<=9; i++)); do
						echo $alpha " " $beta " " $gamma " " $i " " \
						`zgrep "RES VNF" ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | wc -l` " " \
						`zgrep "RES VNF" ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | awk '//{print $3}' | awk -f average.awk` " " \
						`zgrep "RES VNF" ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | awk '//{print $4}' | awk -f average.awk` " " \
						`zgrep "RES VNF" ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | awk '//{print $5}' | awk -f average_t.awk` " " \
						`zgrep "RES NO" ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | wc -l` " " \
						`zgrep "RES NO" ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | awk '//{print $3}' | awk -f average.awk` " " \
						`zgrep "RES NO" ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | awk '//{print $4}' | awk -f average.awk` " " \
						`zgrep "RES NO" ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log.gz | awk '//{print $5}' | awk -f average_t.awk` " " \
		;
					#
					#	./simulate ../"$s"/m_eu_"$s"_"$i"_prob $i > ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log
					#	gzip ./results/m_eu_"$s"_"$i"_test_"$alpha"_"$beta"_"$gamma"_"$cplex".log
					done;
				done;
			done;
		done;
	done;
done

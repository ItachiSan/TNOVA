#!/bin/sh
standard_bz2="results_serialized.log.bz2"
standard_csv="standard_results.csv"
naive_bz2="results_serialized_naive.log.bz2"
naive_csv="naive_results.csv"
perfect_bz2="results_serialized_perfect.log.bz2"
perfect_csv="perfect_results.csv"


if [ -z $1 ]; then
	echo "Usage: $0 RESULTS_DIR"
	echo "Create a directory with proper CSVs of the tests results."
	echo "Search recursively in the directory of invocation (now: $PWD)"
else
	#echo "accepted_accepted;accepted_refused;refused_accepted;refused_refused" > $1
	mkdir -p $1
	# Create CSV for standard tests
	find $PWD -name $standard_bz2 | head -n1 | xargs bzcat | head -n1 > "$1/$standard_csv"
	find $PWD -name $standard_bz2 -exec bzcat {} \; | grep -v "model" | sort >> "$1/$standard_csv"
	# Create CSV for naive tests
	find $PWD -name $naive_bz2 | head -n1 | xargs bzcat | head -n1 > "$1/$naive_csv"
	find $PWD -name $naive_bz2 -exec bzcat {} \; | grep -v "model" | sort >> "$1/$naive_csv"
	# Create CSV for perfect tests
	find $PWD -name $perfect_bz2 | head -n1 | xargs bzcat | head -n1 > "$1/$perfect_csv"
	find $PWD -name $perfect_bz2 -exec bzcat {} \; | grep -v "model" | sort >> "$1/$perfect_csv"
fi

/*
	Solver related methods.
	Function definitions.
*/
#include <fstream>
#include <iostream>
#include "io.h"
#include "simulate.h"
#include "solvers.h"
#include "substrate.h"
#include "vnf.h"

using namespace std;

bool get_solution(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats) {
	int i,j;
	int foo;
	int a_cnt;
	four f;
	
	ifstream tnova("TNOVA.sol");

	tnova >> stats.system_time;
	tnova >> stats.user_time;
	tnova >> stats.clock_time;
	for (i = 0; i<vnf->N; i++) for (j = 0; j<substrate->N; j++) {
		tnova >> foo;
		//cout << foo;
		if (tnova.fail()) return false;
		if (foo == 1) y[i] = j; // Associo al i-esimo VNF il nodo j dell'infrastruttura (soluzione)
	}
	tnova >> a_cnt;
	if (tnova.fail()) return false;
	for (i = 0; i<a_cnt; i++) {
		tnova >> f.p;
		tnova >> f.q;
		tnova >> f.h;
		tnova >> f.k;
		f.display();
		x.push_back(f);
		if (tnova.fail()) return false;
	}
	return true;
}

bool get_glpk_solution(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats) {

	system("rm TNOVA.sol; /usr/bin/time -f \"\%S \%U \%e\" --output=TNOVA.sol glpsol -y TNOVA_t.sol --model TNOVA.mod --data TNOVA.dat --tmlim 3; cat TNOVA_t.sol >> TNOVA.sol");
//	system("/usr/bin/time -f \"\%S \%U \%e\" --output=TNOVA.sol glpsol -y TNOVA_t.sol --model TNOVA.mod --data TNOVA.dat --mipgap 0.01 --tmlim 60; cat TNOVA_t.sol >> TNOVA.sol");

	return get_solution(substrate, vnf, y, x, stats);
}

bool get_cplex_solution(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats) {
	int i,j;
	int foo;

	system("rm TNOVA.sol; /usr/bin/time -f \"\%S \%U \%e\" --output=TNOVA.sol ampl TNOVA.run; cat TNOVA_t.sol >> TNOVA.sol");
	
	return get_solution(substrate, vnf, y, x, stats);
}

bool solve(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats) {
	int i,j;
	bool res;

	write_dat(substrate, vnf);

	x.clear();
	if (options.use_cplex) res = get_cplex_solution(substrate, vnf, y, x, stats);
	else res = get_glpk_solution(substrate, vnf, y, x, stats);
	if (res) {
		cout << "RES VNF-ALLOCATED " << stats.user_time << " " << stats.system_time << " " << stats.clock_time << endl;
		for (i = 0; i<vnf->N; i++) cout << y[i] << " ";
		cout << endl;
		for (i = 0; i<vnf->N; i++) {
			cout << "substrate node " << y[i] << " cpu was " <<  substrate->v_cpu[y[i]];
			substrate->v_cpu[y[i]] -= vnf->v_cpu[i];
			substrate->v_used_cpu[y[i]] += vnf->v_cpu[i];
			cout << " reducing by " << vnf->v_cpu[i] << " cpu to " << substrate->v_cpu[y[i]] << endl;
		}
		// (p,g,h,k) --> (p,g) are substrate arcs, (h,k) are vnf arcs
		for (list< four >::iterator l = x.begin(); l != x.end(); l++) {
			(*l).display();
			j = 0;
			while ((substrate->a_s[j] != (*l).p || substrate->a_t[j] != (*l).q) && j < substrate->A) j++;
			if (j == substrate->A) cout << "SS ERROR" << endl;
			i = 0;
			while ((vnf->a_s[i] != (*l).h || vnf->a_t[i] != (*l).k) && i < vnf->A) i++;
			if (i == vnf->A) cout << "VNF ERROR" << endl;
			cout << "substrate link " << substrate->a_s[j] << "," << substrate->a_t[j] << " bandwidth was " <<  substrate->a_bandwidth[j];
			substrate->a_bandwidth[j] -= vnf->a_bandwidth[i];
			cout << " reducing with " << vnf->a_s[i] << "," << vnf->a_t[i] << " by " << vnf->a_bandwidth[i] << " bandwidth to " << substrate->a_bandwidth[j] << endl;
		}
		/*
		for (i = 0; i<vnf->A; i++) {
			j = 0;
			while (substrate->a_s[j] != sol[vnf->a_s[i]] && substrate->a_t[j] != sol[vnf->a_t[i]]) j++;
			cout << "substrate link " << substrate->a_s[j] << "-" << substrate->a_t[j] << " bandwidth was " <<  substrate->a_bandwidth[j];
			substrate->a_bandwidth[j] -= vnf->a_bandwidth[i];
			cout << " reducing by " << vnf->a_bandwidth[i] << " bandwidth to " << substrate->a_bandwidth[j] << endl;
			if (substrate->a_bandwidth[j] < 0) abort();
		}
		*/
		return true;
	} else {
		cout << "RES NO-FEASIBLE-ALLOCATION " << stats.user_time << " " << stats.system_time << " " << stats.clock_time << endl;
		return false;
	}
	//substrate->display();
	//delete vnf;

}

/*
	Header for methods for reading/writing files.
*/
#pragma once
#include <vector>
#include "simulate.h"
#include "substrate.h"
#include "vnf.h"

double read(char * filename, Substrate * & substrate, vector<VNF *>& vnf_list);
void write_dat(Substrate * substrate, VNF * vnf);

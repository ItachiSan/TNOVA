#pragma once
#include <string>
#include <vector>
using std::string;
using std::vector;

double get_csv_value(string filename, int line_number, int first_valid_line,
	int valid_lines_number, int field);
vector<double> create_array_from_csv(string filename, int first_valid_line,
	int valid_lines_number, int field);

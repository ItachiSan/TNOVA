#include <fstream>
#include <iostream>
#include <string>
#include "csv.h"
using std::string;
using std::stod;
using std::ifstream;
using std::cerr;
using std::endl;

double get_csv_value(string filename,
		int line_number,
		int first_valid_line,
		int valid_lines_number,
		int field) {
	ifstream in_file(filename);
	int i = 1, j = 1;
	string s;
	// Discard unwanted strings
	while (i < (first_valid_line + (line_number % valid_lines_number)) && in_file.good()){
		getline(in_file,s);
		i++;
	}
	// Get the first part...
	while(j < field) {
		getline(in_file, s, ';');
		j++;
	}
	// And save last part
	getline(in_file, s, ';');
	// Return then it as a double.
	//cerr << "csv.cpp: got value " << s << endl;
	return stod(s);
}

vector<double> create_array_from_csv(string filename,
		int first_valid_line,
		int valid_lines_number,
		int field) {
	vector<double> v;
	ifstream in_file(filename);
	int i = 1, j = 1;
	string s;
	// Discard unwanted strings
	while (i < first_valid_line && in_file.good()){
		getline(in_file,s);
		i++;
	}
	// Create the array
	i = 0;
	while (i < valid_lines_number && in_file.good()){
		// Discard unwanted fields
		j=1;
		while(j < field) {
			getline(in_file, s, ';');
			j++;
		}
		// Get what we want
		getline(in_file, s, ';');
		// Add it to the vector
		//cerr << "csv.cpp: got value " << s << endl;
		v.push_back(stod(s));
		// Get to next line.
		getline(in_file,s);
		i++;
	}
	// Return the vector we created.
	return v;
}

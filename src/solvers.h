/*
	Solver related methods.
	Function definitions.
*/
#pragma once
#include "types.h"

bool get_solution(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats);
bool get_glpk_solution(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats);
bool get_cplex_solution(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats);
bool solver(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats);

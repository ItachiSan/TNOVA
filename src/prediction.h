#pragma once
#include <string>
#include "csv.h"
#include "events.h"
#include "substrate.h"
#include "types.h"
#include "vnf.h"
using std::string;

bool solve_in_future_old(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats, double shift,
	string prediction_file, int start_line,	int max_predictions, int field);
bool solve_old(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats, double shift,
	string prediction_file, int start_line,	int max_predictions, int field);
bool solve_in_future(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats,
	vector<double>values, double shift, event_queue events);
bool solve(Substrate * substrate, VNF * vnf, int * y, list< four >& x, Stats& stats,
	vector<double>values, double shift);

/*
	Structure related to the options.
	Structure definition.
*/
#pragma once
#include <string>
using std::string;

struct Options {

	bool stress_test; // Funzionamento stress test o normale
	bool use_cplex; // Uso CPLEX o GLPK

	// Parametri nella funzione obiettivo del modello
	double ALPHA;
	double BETA;
	double GAMMA;

	double avg_load; // Target di carico medio dei DC
	double sim_length; // Durata della simulazione
	double external_load; // External load from other services

	// Data needed for parsing the CSV file
	string prediction_file; // The file to read
	int	start_line; // The first line with a valid value
	int predictions_amount; // The number of amounts we have
	int present_field; // Field index in the CSV to use for present problem solutions
	int future_field; // Field index in the CSV to use for future problem solutions
	double meanCPU_max; // Maximum value of mean CPU in the CSV

	void read(string infile);
};

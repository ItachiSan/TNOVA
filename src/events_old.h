/*
	Events class.
	Structure of the class.
*/
#pragma once
struct ReleaseEvent{
	double time; // Momento in cui le risorse devono essere rilasciate

	int N; // Numero di nodi
	int A; // Numero di archi

	// Per ogni nodo del substrato a cui e` stato allocato un nodo della VNF
	int * v_id;
	int * v_cpu;
	
	// Per ogni link del substrato su cui e` stato allocato un link di una VNF
	int * a_s;
	int * a_t;	
	int * a_bandwidth;

	// Methods.
	ReleaseEvent(int _N, int _A);
	void display();
	~ReleaseEvent();
};

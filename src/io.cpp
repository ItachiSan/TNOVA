/*
	General functions to read/write files.
*/
#include <fstream>
#include <iostream>
#include "io.h"
#define INCLUDE_COMPATIBILITY
using namespace std;

double read(char * filename, Substrate * & substrate, vector<VNF *>& vnf_list) {
	ifstream in_file(filename);
	VNF * vnf;
	int i, k, N, A, N_VNF;

	// LETTURA DA FILE ED INIZIALIZZAZIONE STRUTTURE
	in_file >> N;
	in_file >> A;

	substrate = new Substrate(N,A);

	for (i = 0; i< substrate->N; i++) {
		in_file >> substrate->v_id[i];
		in_file >> substrate->v_cost[i];
		in_file >> substrate->v_cpu[i];
		in_file >> substrate->v_group[i];
	}
	for (i = 0; i< substrate->A; i++) {
		in_file >> substrate->a_s[i];
		in_file >> substrate->a_t[i];
		in_file >> substrate->a_bandwidth[i];
		in_file >> substrate->a_cost[i];
		in_file >> substrate->a_delay[i];
	}


	in_file >> N_VNF;

	vnf_list.resize(N_VNF);

	for (k = 0; k<N_VNF; k++) {
		in_file >> N;
		in_file >> A;

		vnf = new VNF(N,A);
		for (i = 0; i< vnf->N; i++) {
			in_file >> vnf->v_id[i];
			in_file >> vnf->v_cpu[i];
		}
		for (i = 0; i< vnf->A; i++) {
			in_file >> vnf->a_s[i];
			in_file >> vnf->a_t[i];
			in_file >> vnf->a_bandwidth[i];
			in_file >> vnf->a_delay[i];
		}
		in_file >> vnf->I;
		int vnf_id, substrate_id;
		for (i = 0; i<vnf->I; i++) {
			in_file >> vnf_id;
			in_file >> substrate_id;
			vnf->compatibility.push_back(pair<int,int>(vnf_id,substrate_id));
		}
		in_file >> vnf->substrate_size;
		in_file >> vnf->vnf_type;

		vnf_list[k] = vnf;
	}
	// FINE LETTURE

	// CALCOLO STATISTICHE
	double total_cpu_avail = 0.0; // CPU totale disponibile nei DC
	for (i = 0; i<substrate->N; i++) total_cpu_avail += substrate->v_cpu[i];

	double total_cpu_demand = 0.0; // richiesta totale di CPU
	for (k = 0; k<N_VNF; k++) {
		for (i = 0; i<vnf_list[k]->N; i++) total_cpu_demand += vnf_list[k]->v_cpu[i];
	}
	
	double total_bw_avail = 0.0; // bandwidth disponibile
	for (i = 0; i<substrate->A; i++) total_bw_avail += substrate->a_bandwidth[i];
	double total_bw_demand = 0.0; // bandwidth richiesta
	for (k = 0; k<N_VNF; k++) for (i = 0; i<vnf_list[k]->A; i++) total_bw_demand += vnf_list[k]->a_bandwidth[i];
	
	// stampa
	cout << "STAT total_cpu_avail " << total_cpu_avail << " total_cpu_demand " << total_cpu_demand / N_VNF << " total_bw_avail " << total_bw_avail << " total_bw_demand " << total_bw_demand << endl; 

	// Stampa di statistiche sul dataset VNF usato
	#ifdef DELIV
	double avg_vnf_nodes = 0.0;
	for (k = 0; k<N_VNF; k++) avg_vnf_nodes += vnf_list[k]->N;
	avg_vnf_nodes /= N_VNF;
	int vnf_types = 0;
	for (k = 0; k<N_VNF; k++) if (vnf_list[k]->vnf_type > vnf_types) vnf_types = vnf_list[k]->vnf_type;
	double avg_feasible = 0.0;
	for (k = 0; k<N_VNF; k++) {
		avg_feasible += (double) vnf_list[k]->compatibility.size() / (substrate->N * vnf_list[k]->N);
	}
	avg_feasible /= N_VNF;
	cout << "DELIV " << substrate->N << " " << avg_vnf_nodes << " " << vnf_types << " " << avg_feasible << " " << total_cpu_avail / total_cpu_demand << endl;
	abort();
	#endif

	// restituisce il numero medio di nodi di VNF che possono essere ospitati sullo stesso nodo del NS
	// considerando *solo* la CPU disponibile
	return total_cpu_avail / (total_cpu_demand / N_VNF);
}

void write_dat(Substrate * substrate, VNF * vnf) {
	int i,j;
	ofstream tnova("TNOVA.dat");

	tnova << "data; " << endl;

	tnova << "set NInodes := ";
	for (i = 0; i< substrate->N; i++) tnova << substrate->v_id[i] << " ";
	tnova << ";" << endl;

	tnova << "set NIlinks := ";
	for (i = 0; i< substrate->A; i++) tnova << "(" << substrate->a_s[i] << "," << substrate->a_t[i] << ") ";
	tnova << ";" << endl;

	tnova << "set VNFnodes := ";
	for (i = 0; i< vnf->N; i++) tnova << vnf->v_id[i] << " ";
	tnova << ";" << endl;

	tnova << "set NSlinks := ";
	for (i = 0; i< vnf->A; i++) tnova << "(" << vnf->a_s[i] << "," << vnf->a_t[i] << ") ";
	tnova << ";" << endl;

	tnova << "set IndexDelayPaths := ";
	for (i = 0; i< vnf->A; i++) tnova << i << " ";
	tnova << ";" << endl;
	for (i = 0; i< vnf->A; i++) {
		tnova << "set PD[" << i << "] := ";
		tnova << "(" << vnf->a_s[i] << "," << vnf->a_t[i] << ") ";
		tnova << ";" << endl;
	}

	tnova << "set NT := cpu;" << endl;
	tnova << "set LT := bandwidth;" << endl;

	tnova << "param alpha := " << options.ALPHA << ";" << endl;
	tnova << "param beta := " << options.BETA << ";" << endl;
	tnova << "param gamma := " << options.GAMMA << ";" << endl;

	tnova << "param:\tResourceLinkCapacity :=" << endl;
	for (i = 0; i<substrate->A; i++) tnova << substrate->a_s[i] << " " << substrate->a_t[i] << " bandwidth " << substrate->a_bandwidth[i] << endl;
	tnova << ";" << endl;

	tnova << "param:\tResourceLinkDemand :=" << endl;
	for (i = 0; i<vnf->A; i++) tnova << vnf->a_s[i] << " " << vnf->a_t[i] << " bandwidth " << vnf->a_bandwidth[i] << endl;
	tnova << ";" << endl;

	tnova << "param:\tResourceNodeCapacity :=" << endl;
	for (i = 0; i<substrate->N; i++) tnova << substrate->v_id[i] << " cpu " << substrate->v_cpu[i] << endl;
	tnova << ";" << endl;
	
	tnova << "param:\tResourceNodeDemand :=" << endl;
	for (i = 0; i<vnf->N; i++) tnova << vnf->v_id[i] << " cpu " << vnf->v_cpu[i] << endl;
	tnova << ";" << endl;
	
	tnova << "param:\tLinkDelay :=" << endl;
	for (i = 0; i<substrate->A; i++) tnova << substrate->a_s[i] << " " << substrate->a_t[i] << " " << substrate->a_delay[i] << endl;
	tnova << ";" << endl;

	tnova << "param:\tMaxDelay :=" << endl;
	for (i = 0; i<vnf->A; i++) tnova << i << " " << vnf->a_delay[i] << endl;
	tnova << ";" << endl;

	tnova << "param:\tc :=" << endl;
	for (j = 0; j<substrate->N; j++) for (i = 0; i<vnf->N; i++) tnova << vnf->v_id[i] << " " << substrate->v_id[j] << "\t" << substrate->v_cost[j] << endl;
	tnova << ";" << endl;

	tnova << "param:\tlbound :=" << endl;
	tnova << ";" << endl;
	tnova << "param:\tubound :=" << endl;
	#ifdef INCLUDE_COMPATIBILITY
	for (list< pair<int,int> >::iterator l = vnf->compatibility.begin(); l != vnf->compatibility.end(); l++) tnova << (*l).first << " " << (*l).second << " 1" << endl;
	#endif
	tnova << ";" << endl;

	tnova << "end;" << endl;

	tnova.close();
}

/*
	Events class.
	Methods of the class.
*/
#include <fstream>
#include <iostream>
#include "events.h"
using namespace std;

Event::Event(int _N, int _A) {
	N = _N;
	A = _A;
	type = REQUEST_NEW;
	v_id = new int[N];
	v_cpu = new int[N];

	a_s = new int[A];
	a_t = new int[A];
	a_bandwidth = new int[A];
}

void Event::display() {
	int i;
	cout << N << " " << A << " " << start_time << endl;
	for (i = 0; i<N; i++) cout <<  v_id[i] << " " << v_cpu[i] << " " << endl;
	for (i = 0; i<A; i++) cout <<  a_s[i] << " " << a_t[i] << " " << a_bandwidth[i] << endl;
}

void Event::resize(int size){
	A = size;
	delete[] a_s;
	delete[] a_t;
	delete[] a_bandwidth;
	a_s = new int[size];
	a_t = new int[size];
	a_bandwidth = new int[size];
}

Event::~Event() {
	cout << "Destroying event related to VNF " << vnf_index << endl;
	delete[] v_id;
	delete[] v_cpu;

	delete[] a_s;
	delete[] a_t;
	delete[] a_bandwidth;
}

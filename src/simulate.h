/*
	Defines of data used by the simulator but also from other components.
*/
#pragma once
#include "options.h"

extern Options options; // Read the options
extern double LAMBDA;

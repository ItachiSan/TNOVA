/*
	Custom structures for the project.
	Datatype and structures definitions.
*/
#include <fstream>
#include <iostream>
#include "types.h"
using namespace std;

four::four() {
	p = q = h = k = 0;
}

four::four(int _p, int _q, int _h, int _k) {
	p = _p;
	q = _q;
	h = _h;
	k = _k;
}

four::four(const four& inf) {
	p = inf.p;
	q = inf.q;
	h = inf.h;
	k = inf.k;
}

void four::display() {
	cout << p << " " << q << " " << h << " " << k << endl;
}

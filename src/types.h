/*
	Custom structures for the project.
	Datatype and structures definitions.
*/
#pragma once

struct Stats {
	double user_time;
	double system_time;
	double clock_time;
};

struct four {
	int p, q, h, k;

	four ();
	four(int _p, int _q, int _h, int _k);
	four(const four& inf);
	void display();
};

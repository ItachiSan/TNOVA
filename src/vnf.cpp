/*
	Virtual Network Function.
	Methods definition.
*/
#include <fstream>
#include <iostream>
#include <list>
#include "vnf.h"

VNF::VNF(int _N, int _A) {
	N = _N;
	A = _A;
	v_id = new int[N];
	v_cpu = new int[N];

	a_s = new int[A];
	a_t = new int[A];
	a_bandwidth = new int[A];
	a_delay = new int[A];
}

VNF::~VNF() {
	delete[] v_id;
	delete[] v_cpu;

	delete[] a_s;
	delete[] a_t;
	delete[] a_bandwidth;
	delete[] a_delay;

	compatibility.clear();
}
	
void VNF::display() {
	int i;
	cout << N << " " << A << endl;
	for (i = 0; i<N; i++) cout <<  v_id[i] << " " << " " << v_cpu[i] << endl;
	for (i = 0; i<A; i++) cout <<  a_s[i] << " " << a_t[i] << " " << a_bandwidth[i] << " " << a_delay[i] << endl;
	for (list< pair<int,int> >::iterator l = compatibility.begin(); l != compatibility.end(); l++) cout << (*l).first << " " << (*l).second << endl;
}

/*
	Substrate.
	Structure definition.
*/
#pragma once

struct Substrate {
	int N; // numero nodi
	int A; // numero archi

	// Per ogni nodo:
	int * v_id;
	int * v_cost;
	int * v_cpu;
	int * v_used_cpu; // To keep track of the used CPU
	int * v_group; // ID gruppo DC
	double * v_k; // Constant factor, equal to v_cpu/meanCPU_max
	int * v_offset; // Offset for line in CSV file

	// Per ogni arco
	int * a_s; // sorgente
	int * a_t; // destinazione	
	int * a_bandwidth;
	int * a_cost;
	int * a_delay;

	Substrate(int _N, int _A);
	Substrate(Substrate * s);
	~Substrate();
	void display();
};

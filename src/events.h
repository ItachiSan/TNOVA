/*
	Events class.
	Structure of the class.
*/
#pragma once
#include <queue>
#include <vector>
using std::priority_queue;
using std::vector;

typedef enum {
	REQUEST_NEW,
	REQUEST_ACCEPT,
	REQUEST_REJECT,
	REQUEST_FREE
} event_type;

struct Event {
	double creation_time; // Momento in cui la richiesta viene creata
	double start_time; // Momento in cui la richiesta inizia
	double end_time; // Momento in cui le risorse devono essere rilasciate
	event_type type; // Tipologia dell'evento
	int vnf_index; // Index of the VNF in the VNFs list.

	int N; // Numero di nodi
	int A; // Numero di archi

	// Per ogni nodo del substrato a cui e` stato allocato un nodo della VNF
	int * v_id;
	int * v_cpu;

	// Per ogni link del substrato su cui e` stato allocato un link di una VNF
	int * a_s;
	int * a_t;	
	int * a_bandwidth;

	// Methods.
	Event(int _N, int _A);
	void display();
	void resize(int size);
	~Event();

	// Overloaded operators.
	bool operator<(Event e) const {
		return start_time < e.start_time;
	}
};

// Struct present for comparison purpouse
struct minEventHeap {
	bool operator()(Event* a, Event* b) {
		//std::cerr << "Event comparison: " << a-> start_time << " > " << b->start_time << std::endl;
		return a->start_time > b->start_time;
	}
};

// Type name for our event priority queue.
typedef priority_queue<Event *, vector<Event *>, minEventHeap> event_queue;

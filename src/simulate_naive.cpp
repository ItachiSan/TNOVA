/*
	The main program, which generates proper data for the solvers and simulate
	VNFs allocation over a real datacenter network (the substrate).
*/
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <queue>
#include <random>
#include <string>
#include <vector>
#include "csv.h"
#include "events_old.h"
#include "io.h"
#include "simulate.h"
#include "solvers_old.h"
#include "substrate.h"
#include "types.h"
#include "vnf.h"
using namespace std;

// Some custom definitions.
#define INCLUDE_COMPATIBILITY
//#define USE_CPLEX
//#define STRESS_TEST
//#define LAMBDA 2.4 
#define D_MEAN 24.0
#define D_STDDEV 2.0
//#define DELIV

// Define the values we need
Options options;
double LAMBDA;

/*
TODO
Different approach for
- new request
- evaluation of accepted event
- evaluation of not accepted event
*/
void simulate(
	Substrate * substrate, // risorse disponibili sui DC
	vector<VNF *> vnf_list,  // pool di VNF
	double T, // time limit della simulazione
	int seed, // seed per generatori casuali
	vector<double> values, // Values of the datacenters CPU load
	char * model_name // Name of input model
	) {
	// recuperare soluzioni da GLPK
	int * y;
	list< four > x;
	Stats stats;

	// tracking simulatore
	double current_T; // tempo corrente
	int current_VNFs; // numero di VNF correntemente allocate al substrato
	int current_iter; // iterazione corrente
	double next_event_time; // tempo a cui si verifica il prossimo evento
	double duration; // durata della VNF corrente
	int next_vnf; // indice della VNF corrente
	VNF * vnf; // puntatore alla VNF corrente

	// gestione eventi release
	list<ReleaseEvent *> release_events;
	list<ReleaseEvent *>::iterator next_events;

	// generatori casuali
	int i,j;
	default_random_engine generator(seed); // generatore casuale
	exponential_distribution<double> exp_distrib(1.0/LAMBDA);
	normal_distribution<double> normal_distrib(D_MEAN,D_STDDEV);
	uniform_int_distribution<int> unif_distrib(0,vnf_list.size() - 1);

	/*
	Previously, simulation had as maximum 168 time units.
	To make it more human-like, convert these 168 units to a period of a week
	(1440 minutes per day for 7 days).
	*/
	double time_normalize_costant = ((1440.0 * 7.0) / 168.0);

	// Simulator log files.
	ofstream new_events_log("new_events_naive.log");
	ofstream clear_events_log("free_requests_naive.log");
	ofstream results("results_naive.log");
	ofstream results_serialized("results_serialized_naive.log");

	int maxN = 0;
	for(i = 0; i< vnf_list.size(); i++) maxN = max(maxN, vnf_list[i]->N);
	y = new int[maxN];

	current_T = 0.0; current_VNFs = 0; current_iter = 0;

	// scrivi nel log i parametri della simulazione
	cout << T << " " << seed << endl;

	// Accepted/refused counters.
	int accepted = 0, refused = 0;

	// ripeti fino a raggiungere il time limit
	while (current_T < T) {

		if (options.stress_test) // Stress test per solver: arrivano e non le dealloco mai
			if (current_iter >= vnf_list.size()) break;

		// porta next_events al tempo corrente
		for(next_events = release_events.begin(); next_events != release_events.end() && (*next_events)->time < current_T; next_events++);

		// Choose inter-arrival time at random (Poisson Process)
		if (options.stress_test) 
			next_event_time = 0.0;
		else
			/*
			TODO
			Find the next time to start the work.
			It should be the minimum of:
			- the next request event
			- the start of the next accepted event
			- the start of the next refused event
			*/
			// estraggo il tempo che manca dal prossimo evento da distribuzione esponenziale
			next_event_time = exp_distrib(generator) * time_normalize_costant;
		
		current_T += next_event_time;

		// Update substrate with releases
		while (next_events != release_events.end() && (*next_events)->time < current_T) {
			current_VNFs--;
			clear_events_log << "Release event at time " << (*next_events)->time << " current VNFs " << current_VNFs << endl;
			for(i = 0; i<(*next_events)->N; i++) {
				substrate->v_cpu[(*next_events)->v_id[i]] += (*next_events)->v_cpu[i];
				substrate->v_used_cpu[(*next_events)->v_id[i]] -= (*next_events)->v_cpu[i];
			}
			for(i = 0; i<(*next_events)->A; i++) {
				j = 0;
				while ( substrate->a_s[j] != (*next_events)->a_s[i] || substrate->a_t[j] != (*next_events)->a_t[i] ) j++;
				substrate->a_bandwidth[j] += (*next_events)->a_bandwidth[i];
			}
			next_events++;
		}

		// Load CPU
		for (i=0; i < substrate->N; i++) {
			// Be sure to be in the proper range
			j = (((int) current_T) + substrate->v_offset[i]) % values.size();
			// Calculate the new CPU value of the node
			substrate->v_cpu[i] = values[j]	* substrate->v_k[i];
			// Remove used CPU units.
			substrate->v_cpu[i] -= substrate->v_used_cpu[i];
		}

		// Choose VNF at random (Uniformly)
		if (options.stress_test) 
			next_vnf = current_iter;
		else
			next_vnf = unif_distrib(generator);

		vnf = vnf_list[next_vnf];

		/* TODO
		For each substrate node, predict the future load of the nodes.
		To do this:
		- Read the actual load of the node = what I keep in the substrate + the real data
		- From the prevision model, look at the future load of the machine
		- Generate the future substrate where each node has load equal to substrate value + future data
		*/

		// In ANY CASE choose duration at random (Normal distrib.)
		if (options.stress_test) 
			duration = 1000.0;
		else
			duration = normal_distrib(generator) * time_normalize_costant;

		//cout << next_vnf << " " << current_T << " " << duration << endl;
		new_events_log << "VNF " << next_vnf;
		new_events_log << " allocation request at time " << current_T;
		new_events_log << ", duration " << duration << endl;

		// Solve allocation problem
		if (solve(substrate, vnf, y, x, stats)) {
			
			// sono riuscito ad allocare la richiesta corrente al substrato
			current_VNFs++;
			accepted++;
			
			cout << "VNF duration " << duration << " release time " << current_T + duration << " current VNFs " << current_VNFs << endl;

			// Crea l'evento release corrispondente
			ReleaseEvent * event = new ReleaseEvent(vnf->N, x.size());
			event->time = current_T + duration;
			for(i = 0; i<vnf->N; i++) {
				event->v_id[i] = y[i];
				event->v_cpu[i] = vnf->v_cpu[i];
			} 
			// (p,g,h,k) --> (p,g) are substrate arcs, (h,k) are vnf arcs
			j = 0;
			for (list< four >::iterator l = x.begin(); l != x.end(); l++, j++) {
				event->a_s[j] = (*l).p;
				event->a_t[j] = (*l).q;
				i = 0;
				while ((vnf->a_s[i] != (*l).h || vnf->a_t[i] != (*l).k) && i < vnf->A) i++;
				event->a_bandwidth[j] = vnf->a_bandwidth[i]; 
			}

			// inserimento ordinato nella lista release_events
			list<ReleaseEvent *>::iterator m;
			for(m = next_events; m != release_events.end() && (*m)->time < event->time; m++);
			release_events.insert(m,event);
		} else {
			refused++;
		}
		current_iter++;
	}

	cout << "Simulation finished! VNFs still allocated: " << current_VNFs;
	cout << ", events handled: " << current_iter << endl;
	cout << "Writing to results files..." << endl;

	// Create results logs
	// Human one
	results << "Simulator: naive" << endl;
	results << "Stress test: " << (options.stress_test ? "true" : "false") << endl;
	results << "Using CPLEX: " << (options.use_cplex ? "true" : "false") << endl;
	results << "" << endl;
	results << "Problem variables" << endl;
	results << "Alpha: " << options.ALPHA << endl;
	results << "Beta: " << options.BETA << endl;
	results << "Gamma: " << options.GAMMA << endl;
	results << "" << endl;
	results << "Average load: " << options.avg_load << endl;
	results << "Simulation time: " << options.sim_length << endl;
	results << "Maximum mean CPU load: " << options.meanCPU_max << endl;
	results << "" << endl;
	results << "Requests status" << endl;
	results << "Accepted = " << accepted << endl;
	results << "Refused = " << refused << endl;
	// Machine (CSV) one
	results_serialized << "model;size;alpha;beta;gamma;";
	results_serialized << "accepted;refused;" << endl;
	results_serialized << model_name << ";";
	results_serialized << substrate->N << ";";
	results_serialized << options.ALPHA << ";" << options.BETA << ";";
	results_serialized << options.GAMMA << ";";
	results_serialized << accepted << ";" << refused << endl;

	x.clear();
	delete[] y;
}

int main(int argc, char ** argv) {
	Substrate * substrate; // Descrizione della rete di DC
	vector<VNF *> vnf_list; // Pool di VNF -> Decrizione del pool di NS
	int i,j;
	double max_vnf_load; // Numero medio di nodi di VNF che possono essere caricati sullo stesso nodo NS

	max_vnf_load = read(argv[1], substrate, vnf_list); // Carica dati su DC e VNF

	options.read("options.dat");

	// Set k and offset for each node of the substrate
	srand(time(NULL));
	cout << "Max mean CPU: " << options.meanCPU_max << endl;
	for (i = 0; i < substrate->N; i++){
		cout << "Start CPU for node " << i << ": " << (double) substrate->v_cpu[i] << endl;
		cout << "K for node " << i << ": " << ((double) substrate->v_cpu[i]) / options.meanCPU_max << endl;
		substrate->v_k[i] = (((double) substrate->v_cpu[i]) * (1.0 - options.external_load)) / options.meanCPU_max;
		substrate->v_offset[i] = rand() % options.predictions_amount;
	}

	cout << "Initial substrate status" << endl;
	substrate->display();
	//vnf->display();

	//LAMBDA = D_MEAN / (atof(argv[3]) * vnf_list.size());
	// D_MEAN --> durata media di una richiesta di allocazione
	// LAMBDA e` tale da avere le richieste di allocazione che si sovrappongono in un numero tale
	// da rendere il carico medio pari a quello richiesto in input al simulatore
	LAMBDA = D_MEAN / (options.avg_load * max_vnf_load);

	cout << "Interarrival: " << LAMBDA << endl;

	// Generate the vectors containing the present and future values of datacenters CPU load
	cout << "Creating CPU values vector" << endl;
	vector<double> values = create_array_from_csv(
		options.prediction_file, options.start_line,
		options.predictions_amount, options.present_field);

	// sottoprogramma principale
	simulate(substrate, vnf_list, options.sim_length, atoi(argv[2]),
		values, argv[1]);

	// pulizia
	for(vector<VNF *>::iterator l = vnf_list.begin(); l != vnf_list.end(); l++) delete (*l);
	vnf_list.clear();
	delete substrate;

	return EXIT_SUCCESS;

}

/*
	The main program, which generates proper data for the solvers and simulate
	VNFs allocation over a real datacenter network (the substrate).
*/
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <list>
#include <queue>
#include <random>
#include <string>
#include <vector>
#include "events.h"
#include "io.h"
#include "prediction.h"
#include "simulate.h"
#include "solvers.h"
#include "substrate.h"
#include "types.h"
#include "vnf.h"
using namespace std;

// Some custom definitions.
//#define INCLUDE_COMPATIBILITY
//#define USE_CPLEX
//#define STRESS_TEST
//#define LAMBDA 2.4 
#define D_MEAN 24.0
#define D_STDDEV 2.0
//#define DELIV

// Define the values we need
Options options;
double LAMBDA;

void simulate(
	Substrate * substrate, // risorse disponibili sui DC
	vector<VNF *> vnf_list,  // pool di VNF
	double T, // time limit della simulazione
	int seed, // seed per generatori casuali
	vector<double> present_values, // Present values of the datacenters CPU load
	vector<double> future_values, // Future values of the datacentes CPU load
	char * model_name // Name of input model
	) {
	// recuperare soluzioni da GLPK
	int * y;
	list< four > x;
	Stats stats;

	// tracking simulatore
	double current_T; // tempo corrente
	int current_VNFs; // numero di VNF correntemente allocate al substrato
	int current_iter; // iterazione corrente
	double next_event_time; // tempo a cui si verifica il prossimo evento
	double next_generated_event_time; // tempo a cui si verifica il prossimo evento generato
	double duration; // durata della VNF corrente
	int next_vnf; // indice della VNF corrente
	VNF * vnf; // puntatore alla VNF corrente

	// Events queue
	event_queue events;

	// generatori casuali
	int i,j;
	// generatore casuale
	default_random_engine generator(seed);
	// Get the time to the next event.
	exponential_distribution<double> exp_distrib(1.0/LAMBDA);
	// Get the next event duration.
	normal_distribution<double> normal_distrib(D_MEAN,D_STDDEV);
	// Choose randomly one of the possible VNFs.
	uniform_int_distribution<int> unif_distrib(0,vnf_list.size() - 1);

	/*
	Time shift from request creation to request start.
	Just for its own sake, we assume that the request starting time
	can be predicted by using a normal distribution.
	We assume that usually the request starts 90 minutes after its creation
	and that the variance is of 30 minutes.
	*/
	normal_distribution<double> time_shift(90.0,30.0);
	/*
	Previously, simulation had as maximum 168 time units.
	To make it more human-like, convert these 168 units to a period of a week
	(1440 minutes per day for 7 days).
	*/
	double time_normalize_costant = ((1440.0 * 7.0) / 168.0);

	// Simulator log files.
	ofstream new_events_log("new_events.log");
	ofstream clear_events_log("free_requests.log");
	ofstream results("results.log");
	ofstream results_serialized("results_serialized.log");
	/*
	Events results counter.
	'a' stands for accepted, 'r' for refused.
	So, a_a stands for 'prevision: accept, result: accept' and so on.
	*/
	int a_a = 0, a_r = 0, r_a = 0, r_r = 0;
	
	// Detect the maximum size of VNFs for having y of the correct size.
	int maxN = 0;
	for(i = 0; i < vnf_list.size(); i++)
		maxN = max(maxN, vnf_list[i]->N);
	y = new int[maxN];

	// Set initial values for important variables, used to keep track of simulation.
	current_T = 0.0; current_VNFs = 0; current_iter = 0;

	// Write the parameter simulation information.
	cout << "Time limit: " << T << ", seed: " << seed << endl;

	// Push an initial "new request" to populate the queue.

	// Detect the first request arrival time.
	next_generated_event_time = time_shift(generator) +
		(exp_distrib(generator) * time_normalize_costant);

	// Choose a VNF for the request and fetch it.
	next_vnf = unif_distrib(generator);
	vnf = vnf_list[next_vnf];

	// Create the new event
	Event *event = new Event(vnf->N, 1);
	event->vnf_index = next_vnf;
	event->creation_time = current_T + next_generated_event_time;

	// As it is a new request, we still don't have start and end time.
	event->start_time = current_T + next_generated_event_time;
	event->end_time = current_T + next_generated_event_time;

	// Write some logs and push the new event in the queue.
	new_events_log << "Event generated at iteration " << current_iter;
	new_events_log << ": " << event->vnf_index << " " << current_T << " ";
	new_events_log << duration << endl;
	events.push(event);


	// Now... Let's simulate!
	while (current_T < T) {
		// Stress test: never removing VNFs
		if (options.stress_test)
			if (current_iter >= vnf_list.size()) break;

		// Get the queue event
		Event *queue_event = events.top();
		events.pop();
		// Update the timer
		current_T = queue_event->start_time;

		// Handle the event
		switch (queue_event->type) {
			case(REQUEST_ACCEPT): {
				// Retrieve the proper VNFs
				vnf = vnf_list[queue_event->vnf_index];

				// Check if we made a good prediction
				if (solve(substrate, vnf, y, x, stats, present_values, queue_event->start_time)) {
					cout << "[Iteration " << current_iter << "] Correct prediction! Predicted: accepted" << endl;
					// Map required stuff in the event
					for(i = 0; i < vnf->N; i++) {
						queue_event->v_id[i] = y[i];
						queue_event->v_cpu[i] = vnf->v_cpu[i];
					}
					// Resize arcs to be correct for the new solution
					queue_event->resize(x.size());
					// (p,g,h,k) --> (p,g) are substrate arcs, (h,k) are vnf arcs
					j = 0;
					//cerr << "Arcs present: " << x.size() << ", expected " << queue_event->A << endl;
					for (list< four >::iterator l = x.begin(); l != x.end(); l++, j++) {
						//cerr << "Arcs: " << queue_event->a_s[j] << " to " << queue_event->a_s[j] << endl;
						queue_event->a_s[j] = l->p;
						queue_event->a_t[j] = l->q;
						i = 0;
						while ((vnf->a_s[i] != l->h || vnf->a_t[i] != l->k) && i < vnf->A) i++;
						queue_event->a_bandwidth[j] = vnf->a_bandwidth[i];
					}
					// Make it a "free resources" request
					queue_event->start_time = queue_event->end_time;
					queue_event->type = REQUEST_FREE;
					// Push it back to the queue
					events.push(queue_event);
					// Mark that we have added VNFs
					current_VNFs++;
					a_a++;
				} else {
					cout << "[Iteration " << current_iter << "] Wrong prediction! Predicted: accepted" << endl;
					delete queue_event;
					a_r++;
				}
				break;
			}

			case(REQUEST_REJECT): {
				// Retrieve the proper VNFs
				vnf = vnf_list[queue_event->vnf_index];

				// Check if we refused properly.
				if (!solve(substrate, vnf, y, x, stats, present_values, queue_event->start_time)) {
					cout << "[Iteration " << current_iter << "] Correct prediction! Predicted: refused" << endl;
					r_r++;
				} else {
					cout << "[Iteration " << current_iter << "] Correct prediction! Predicted: refused" << endl;
					r_a++;
				}
				delete queue_event;
				break;
			}

			case(REQUEST_NEW): {
				// STEP 1: Create a new request.

				// Find out when the request would be allocated.
				if (options.stress_test)
					next_generated_event_time = 0.0;
				else
					next_generated_event_time = time_shift(generator) +
						(exp_distrib(generator) * time_normalize_costant);

				// Choose the allocation time.
				if (options.stress_test)
					duration = 1000.0;
				else
					duration = (normal_distrib(generator) * time_normalize_costant);

				// Update the informations in the event.
				queue_event->start_time = current_T + next_generated_event_time;
				queue_event->end_time = current_T + next_generated_event_time + duration;

				// Fetch the VNF
				vnf = vnf_list[queue_event->vnf_index];

				// Check if we can accept the request or not.
				queue_event->type =
					solve_in_future(substrate, vnf, y, x, stats, future_values,
						queue_event->start_time, events) ?
						REQUEST_ACCEPT : REQUEST_REJECT;

				// Map CPU usage.
				for(i = 0; i < vnf->N; i++) {
					queue_event->v_id[i] = y[i];
					queue_event->v_cpu[i] = vnf->v_cpu[i];
				}

				// Resize arcs to be of the same size of the solution.
				queue_event->resize(x.size());

				// Map arcs usage.
				// (p,g,h,k) --> (p,g) are substrate arcs, (h,k) are vnf arcs
				j = 0;
				for (list< four >::iterator l = x.begin(); l != x.end(); l++, j++) {
					queue_event->a_s[j] = l->p;
					queue_event->a_t[j] = l->q;
					i = 0;
					while ((vnf->a_s[i] != l->h || vnf->a_t[i] != l->k) && i < vnf->A) i++;
					queue_event->a_bandwidth[j] = vnf->a_bandwidth[i];
				}

				// Add to the events queue
				events.push(queue_event);

				// STEP 2: Create a new request arrival after the new request is handled
				// Fetch the new request time
				if (options.stress_test)
					next_generated_event_time = 0.0;
				else
					// Extract the wait time from the next time interarrival
					next_generated_event_time = time_shift(generator) +
						(exp_distrib(generator) * time_normalize_costant);

				// Choose VNF at random (Uniformly)
				if (options.stress_test)
					next_vnf = current_iter;
				else
					next_vnf = unif_distrib(generator);

				// Fetch the VNF
				vnf = vnf_list[next_vnf];

				// Create the new event
				Event *event = new Event(vnf->N, 1);
				event->vnf_index = next_vnf;
				event->creation_time = current_T + next_generated_event_time;

				// As it is a new request, we still don't have start and end time.
				event->start_time = current_T + next_generated_event_time;
				event->end_time = current_T + next_generated_event_time;

				// Write some logs and push the new event in the queue.
				new_events_log << "Event generated at iteration " << current_iter;
				new_events_log << ": " << event->vnf_index << " " << current_T << " ";
				new_events_log << duration << endl;
				events.push(event);
				break;
			}

			case(REQUEST_FREE):
				// Copy-pasted from the old simulator
				clear_events_log << "Release event at time " << queue_event->start_time;
				clear_events_log << ", current VNFs " << current_VNFs << endl;
				for(i = 0; i < queue_event->N; i++) {
					substrate->v_cpu[queue_event->v_id[i]] += queue_event->v_cpu[i];
					substrate->v_used_cpu[queue_event->v_id[i]] -= queue_event->v_cpu[i];
				}
				for(i = 0; i < queue_event->A; i++) {
					j = 0;
					while ( substrate->a_s[j] != queue_event->a_s[i] || substrate->a_t[j] != queue_event->a_t[i] ) j++;
					substrate->a_bandwidth[j] += queue_event->a_bandwidth[i];
				}
				current_VNFs--;
				delete queue_event;
				break;
			}

		// Keep track of loop iteration
		current_iter++;
	}

	// Clean up the priority queue
	while(!events.empty()){
		// Get the queue event
		Event *queue_event = events.top();
		events.pop();
		delete queue_event;
	}

	cout << "Simulation finished! VNFs still allocated: " << current_VNFs;
	cout << ", events handled: " << current_iter << endl;
	cout << "Writing to results files..." << endl;

	// Writing the human-readable results log
	results << "Simulator: standard" << endl;
	results << "Stress test: " << (options.stress_test ? "true" : "false") << endl;
	results << "Using CPLEX: " << (options.use_cplex ? "true" : "false") << endl;
	results << "" << endl;
	results << "Problem variables" << endl;
	results << "Alpha: " << options.ALPHA << endl;
	results << "Beta: " << options.BETA << endl;
	results << "Gamma: " << options.GAMMA << endl;
	results << "" << endl;
	results << "Average load: " << options.avg_load << endl;
	results << "Simulation time: " << options.sim_length << endl;
	results << "" << endl;
	results << "CSV parsing informations" << endl;
	results << "Prediction file used: " << options.prediction_file << endl;
	results << "Maximum mean CPU load: " << options.meanCPU_max << endl;
	results << "External load: " << options.external_load << endl;
	results << "" << endl;
	results << "Requests status" << endl;
	results << "Prevision: accepted, Request: accepted = " << a_a << endl;
	results << "Prevision: accepted, Request: refused = " << a_r << endl;
	results << "Prevision: refused, Request: accepted = " << r_a << endl;
	results << "Prevision: refused, Request: refused = " << r_r << endl;

	// Create the serialized (machine-readable) result log
	results_serialized << "model;size;external_load;csv;alpha;beta;gamma;";
	results_serialized << "accepted_accepted;accepted_refused;";
	results_serialized << "refused_accepted;refused_refused;" << endl;
	results_serialized << model_name << ";";
	results_serialized << substrate->N << ";";
	results_serialized << options.external_load << ";";
	results_serialized << options.prediction_file << ";";
	results_serialized << options.ALPHA << ";" << options.BETA << ";";
	results_serialized << options.GAMMA << ";";
	results_serialized << a_a << ";" << a_r << ";" << r_a << ";" << r_r << endl;

	// Clean up used memory.
	x.clear();
	delete[] y;
}

int main(int argc, char ** argv) {
	Substrate * substrate; // Descrizione della rete di DC
	vector<VNF *> vnf_list; // Pool di VNF -> Decrizione del pool di NS
	int i,j;
	double max_vnf_load; // Numero medio di nodi di VNF che possono essere caricati sullo stesso nodo NS

	max_vnf_load = read(argv[1], substrate, vnf_list); // Carica dati su DC e VNF

	options.read("options.dat");

	// Set k and offset for each node of the substrate
	srand(time(NULL));
	cout << "Max mean CPU: " << options.meanCPU_max << endl;
	for (int i = 0; i < substrate->N; i++){
		cout << "Start CPU for node " << i << ": " << (double) substrate->v_cpu[i] << endl;
		cout << "K for node " << i << ": " << ((double) substrate->v_cpu[i]) / options.meanCPU_max << endl;
		substrate->v_k[i] = (((double) substrate->v_cpu[i]) * (1.0 - options.external_load)) / options.meanCPU_max;
		substrate->v_offset[i] = rand() % options.predictions_amount;
	}

	cout << "Initial substrate status" << endl;
	substrate->display();
	//vnf->display();

	//LAMBDA = D_MEAN / (atof(argv[3]) * vnf_list.size());
	// D_MEAN --> durata media di una richiesta di allocazione
	// LAMBDA e` tale da avere le richieste di allocazione che si sovrappongono in un numero tale
	// da rendere il carico medio pari a quello richiesto in input al simulatore
	LAMBDA = D_MEAN / (options.avg_load * max_vnf_load);

	cout << "Interarrival: " << LAMBDA << endl;

	// Generate the vectors containing the present and future values of datacenters CPU load
	cout << "Creating present values vector" << endl;
	vector<double> present_values = create_array_from_csv(
		options.prediction_file, options.start_line,
		options.predictions_amount, options.present_field);
	cout << "Creating future values vector" << endl;
	vector<double> future_values = create_array_from_csv(
		options.prediction_file, options.start_line,
		options.predictions_amount, options.future_field);

	// sottoprogramma principale
	simulate(substrate, vnf_list, options.sim_length, atoi(argv[2]),
		present_values, future_values, argv[1]);

	// pulizia
	for(vector<VNF *>::iterator l = vnf_list.begin(); l != vnf_list.end(); l++) delete (*l);
	vnf_list.clear();
	delete substrate;
	present_values.clear();
	future_values.clear();

	return EXIT_SUCCESS;
}

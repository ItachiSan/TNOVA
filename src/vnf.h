/*
	Virtual Network Function.
	Structure definition.
*/
#pragma once
#include <list>
using namespace std;

struct VNF {
	int N; // Numero nodi
	int A; // Numero archi
	int I; // Lunghezza della lista di compatibilita

	int substrate_size;
	int vnf_type;

	// Per ogni nodo
	int * v_id;
	int * v_cpu;

	// Per ogni arco
	int * a_s;
	int * a_t;
	int * a_bandwidth;
	int * a_delay;
	list< pair<int, int> > compatibility; // Mappature ammissibili tra nodi della VNF e nodi del substrato
	
	VNF(int _N, int _A);
	void display();
	~VNF();
};

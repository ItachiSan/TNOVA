#include <iostream>
#include "prediction.h"
#include "solvers.h"
using std::cout;
using std::endl;

bool solve_in_future_old(Substrate * substrate,
		VNF * vnf,
		int * y,
		list< four >& x,
		Stats& stats,
		double shift,
		string prediction_file,
		int start_line,
		int max_predictions,
		int field
		) {
	Substrate * future_substrate = new Substrate(substrate);
	cout << "prediction.cpp: printing future substrate, before changes" << endl;
	future_substrate->display();
	for (int i=0; i < future_substrate->N; i++)
		future_substrate->v_cpu[i] =
			get_csv_value(prediction_file,
				((int) shift) + future_substrate->v_offset[i],
				start_line,	max_predictions, field);
	cout << "prediction.cpp: printing future substrate, after changes" << endl;
	future_substrate->display();
	//cerr << "and now the real substrate" << endl;
	//substrate->display();
	bool value = solver(future_substrate, vnf, y, x, stats);
	delete future_substrate;
	return value;
}

bool solve_old(Substrate * substrate,
		VNF * vnf,
		int * y,
		list< four >& x,
		Stats& stats,
		double shift,
		string prediction_file,
		int start_line,
		int max_predictions,
		int field
		) {
	cout << "prediction.cpp: printing substrate, before changes" << endl;
	substrate->display();
	for (int i=0; i < substrate->N; i++)
		substrate->v_cpu[i] =
			get_csv_value(prediction_file,
				((int) shift) + substrate->v_offset[i],
				start_line, max_predictions, field);
	cout << "prediction.cpp: printing substrate, after changes" << endl;
	substrate->display();
	//cerr << "prediction.cpp: not solving with future values!" << endl;
	return solver(substrate, vnf, y, x, stats);
}

bool solve_in_future(Substrate * substrate,
		VNF * vnf,
		int * y,
		list< four >& x,
		Stats& stats,
		vector<double> values,
		double shift,
		event_queue events
		) {
	Substrate * future_substrate = new Substrate(substrate);
	cout << "prediction.cpp: printing future substrate, before changes" << endl;
	future_substrate->display();
	int j;
	for (int i=0; i < future_substrate->N; i++) {
		// Be sure to be in the proper range
		j = (((int) shift) + future_substrate->v_offset[i]) % values.size();
		// Calculate the new CPU value of the node
		future_substrate->v_cpu[i] = values[j] * future_substrate->v_k[i];
		// Remove used CPU units.
		future_substrate->v_cpu[i] -= future_substrate->v_used_cpu[i];
	}
	// Add the future accepted events!
	// Declare variables
	Event * event;
	int i;
	// Loop over the priority queue
	while (!events.empty() && events.top()->start_time <= shift) {
		event = events.top();
		if (event->type == REQUEST_ACCEPT) {
			/*
			Opposite of "request free" in the main code (in simulate.cpp).
			This because the event was already mapped properly, so we don't
			need to make all the checks that are made in the 'solver' function
			(in 'solvers.cpp').
			*/
			for(i = 0; i < event->N; i++)
				future_substrate->v_cpu[event->v_id[i]] -= event->v_cpu[i];
			for(i = 0; i < event->A; i++) {
				j = 0;
				while (future_substrate->a_s[j] != event->a_s[i] || future_substrate->a_t[j] != event->a_t[i] ) j++;
				future_substrate->a_bandwidth[j] += event->a_bandwidth[i];
			}
		}
		events.pop();
	}
	cout << "prediction.cpp: printing future substrate, after changes" << endl;
	future_substrate->display();
	//cerr << "and now the real substrate" << endl;
	//substrate->display();
	bool value = solver(future_substrate, vnf, y, x, stats);
	delete future_substrate;
	return value;
}

bool solve(Substrate * substrate,
		VNF * vnf,
		int * y,
		list< four >& x,
		Stats& stats,
		vector<double> values,
		double shift
		) {
	cout << "prediction.cpp: printing substrate, before changes" << endl;
	substrate->display();
	int j;
	for (int i=0; i < substrate->N; i++) {
		// Be sure to be in the proper range
		j = (((int) shift) + substrate->v_offset[i]) % values.size();
		// Calculate the new CPU value of the node
		substrate->v_cpu[i] = values[j]	* substrate->v_k[i];
		// Remove used CPU units.
		substrate->v_cpu[i] -= substrate->v_used_cpu[i];
	}
	cout << "prediction.cpp: printing substrate, after changes" << endl;
	substrate->display();
	//cerr << "prediction.cpp: not solving with future values!" << endl;
	return solver(substrate, vnf, y, x, stats);
}

/*
	Structure related to the options.
	Methods for the structure.
*/
#include <fstream>
#include <iostream>
#include "options.h"
using namespace std;

void Options::read(string infile) {
	ifstream inf(infile);
	int ioo;
	double foo;
	string soo;
	
		cout << "Reading option file..." << endl;
		inf >> soo;
		cout << soo << endl;
		inf >> ioo;
		if (ioo > 0) stress_test = true;
		else stress_test = false;
		inf >> soo;
		cout << soo << endl;
		inf >> ioo;
		if (ioo > 0) use_cplex = true;
		else use_cplex = false;

		inf >> soo;
		cout << soo << endl;
		inf >> ALPHA;
		inf >> soo;
		cout << soo << endl;
		inf >> BETA;
		inf >> soo;
		cout << soo << endl;
		inf >> GAMMA;
		inf >> soo;
		cout << soo << endl;
		inf >> avg_load;
		inf >> soo;
		cout << soo << endl;
		inf >> sim_length;
		inf >> soo;
		cout << soo << endl;
		inf >> external_load;
		inf >> soo;
		cout << soo << endl;
		inf >> prediction_file;
		inf >> soo;
		cout << soo << endl;
		inf >> start_line;
		inf >> soo;
		cout << soo << endl;
		inf >> predictions_amount;
		inf >> soo;
		cout << soo << endl;
		inf >> present_field;
		inf >> soo;
		cout << soo << endl;
		inf >> future_field;
		inf >> soo;
		cout << soo << endl;
		inf >> meanCPU_max;
};

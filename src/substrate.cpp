/*
	Substrate.
	Class methods.
*/
#include <fstream>
#include <iostream>
#include "substrate.h"
using namespace std;

Substrate::Substrate(int _N, int _A) {
		N = _N;
		A = _A;
		v_id = new int[N];
		v_cost = new int[N];
		v_cpu = new int[N];
		v_used_cpu = new int[N];
		// Used CPU is set to 0 at start.
		for (int i=0; i < N; i++)
			v_used_cpu[i] = 0;
		v_group = new int[N];
		v_k = new double[N];
		v_offset = new int[N];

		a_s = new int[A];
		a_t = new int[A];
		a_bandwidth = new int[A];
		a_cost = new int[A];
		a_delay = new int[A];	
}

Substrate::Substrate(Substrate * s) {
		N = s->N;
		A = s->A;
		v_id = new int[N];
		v_cost = new int[N];
		v_cpu = new int[N];
		v_used_cpu = new int[N];
		v_group = new int[N];
		v_k = new double[N];
		v_offset = new int[N];
		for(int i = 0; i < N; i++) {
			v_id[i] = s->v_id[i];
			v_cost[i] = s->v_cost[i];
			v_cpu[i] = s->v_cpu[i];
			v_used_cpu[i] = s->v_used_cpu[i];
			v_group[i] = s->v_group[i];
			v_k[i] = s->v_k[i];
			v_offset[i] = s->v_offset[i];
		}

		a_s = new int[A];
		a_t = new int[A];
		a_bandwidth = new int[A];
		a_cost = new int[A];
		a_delay = new int[A];
		for(int j = 0; j < A; j++){
			a_s[j] = s->a_s[j];
			a_t[j] = s->a_t[j];
			a_bandwidth[j] = s->a_bandwidth[j];
			a_cost[j] = s->a_cost[j];
			a_delay[j] = s->a_delay[j];
		}
}

Substrate::~Substrate() {
	delete[] v_id;
	delete[] v_cost;
	delete[] v_cpu;
	delete[] v_used_cpu;
	delete[] v_group;
	delete[] v_k;
	delete[] v_offset;

	delete[] a_s;
	delete[] a_t;
	delete[] a_bandwidth;
	delete[] a_cost;
	delete[] a_delay;
}

void Substrate::display() {
	int i;
	cout << N << " " << A << endl;
	for (i = 0; i<N; i++) cout <<  v_id[i] << " " << v_cost[i] << " " << v_cpu[i] << " " << v_used_cpu[i] << " " << v_group[i] << " " << v_k[i] << " " << v_offset[i] << endl;
	for (i = 0; i<A; i++) cout <<  a_s[i] << " " << a_t[i] << " " << a_bandwidth[i] << " " << a_cost[i] << " " << a_delay[i] << endl;
}

/*
	Events class.
	Methods of the class.
*/
#include <iostream>
#include <fstream>
#include "events_old.h"
using namespace std;

ReleaseEvent::ReleaseEvent(int _N, int _A) {
	N = _N;
	A = _A;
	v_id = new int[N];
	v_cpu = new int[N];

	a_s = new int[A];
	a_t = new int[A];
	a_bandwidth = new int[A];		
}

void ReleaseEvent::display() {
	int i;
	cout << N << " " << A << " " << time << endl;
	for (i = 0; i<N; i++) cout <<  v_id[i] << " " << v_cpu[i] << " " << endl;
	for (i = 0; i<A; i++) cout <<  a_s[i] << " " << a_t[i] << " " << a_bandwidth[i] << endl;
}

ReleaseEvent::~ReleaseEvent() {
	delete[] v_id;
	delete[] v_cpu;

	delete[] a_s;
	delete[] a_t;
	delete[] a_bandwidth;
}
